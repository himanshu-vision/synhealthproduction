import { BOOKING_LOAD, BOOKING_UNLOAD } from '../actions';
import { fetchBooking } from '../api/booking';

export function loadMyBooking() {
  return dispatch =>
  fetchBooking()
      .then(payload => {
        dispatch({ type: BOOKING_LOAD, payload: payload.response });
      })
      .catch(payload => {
      });
}
