import { SESSION_LOAD, SESSION_LOGIN, SESSION_LOGOUT } from '../actions';
import { deleteSession, postSession, loginMobile, loginOtp } from '../api/session';
import { updateHeaders } from '../api/utils';

const localStorage = window.localStorage;

export function initialize() {
  return (dispatch) => {
    const { email, name, token } = localStorage;
    console.log('token', token)
    if (token) {
      dispatch({
        type: SESSION_LOAD, payload: { email, name, token }
      });
    } else {
      //window.location = '/login';
    }
  };
}

export function login(email, password, done) {
  return dispatch => (
    postSession(email, password)
      .then((payload) => {
        updateHeaders({ Auth: payload.token });
        dispatch({ type: SESSION_LOGIN, payload });
        try {
          localStorage.email = payload.email;
          localStorage.name = payload.name;
          localStorage.token = payload.token;
        } catch (e) {
          // alert(
          //   'Unable to preserve session, probably due to being in private ' +
          //   'browsing mode.'
          // );
        }
        done();
      })
      .catch(payload => dispatch({
        type: SESSION_LOGIN,
        error: true,
        payload: {
          statusCode: payload.status, message: payload.statusText
        }
      }))
  );
}

export function loginToken(mobile, done) {
  return dispatch => (
    loginMobile(mobile)
      .then((payload) => {
        let payloads = JSON.parse(payload);
        console.log('payloadpayloadpayloadpayload', payloads)
        //updateHeaders({ Auth: payload.token });
       // dispatch({ type: SESSION_LOGIN, payload });
        try {
          
        } catch (e) {
          // alert(
          //   'Unable to preserve session, probably due to being in private ' +
          //   'browsing mode.'
          // );
        }
        done();
      })
      .catch(payload => dispatch({
        type: SESSION_LOGIN,
        error: true,
        payload: {
          statusCode: payload.status, message: payload.statusText
        }
      }))
  );
}



export function otpLogin(mobile, otp, done) {
  return dispatch => (
    loginOtp(mobile, otp)
      .then((payload) => {
        let payloads = JSON.parse(payload);
       
        updateHeaders({ Auth: payloads.response.token });
       // dispatch({ type: SESSION_LOGIN, payloads });
       dispatch({
        type: SESSION_LOAD, payload: { email: payloads.response.customer.email, name: payloads.response.customer.first_name, token: payloads.response.token }
      });
       //localStorage.token = payloads.response.token;
        try {
          localStorage.token = payloads.response.token;
           localStorage.email = payloads.response.customer.email;
           localStorage.name = payloads.response.customer.first_name+' '+payloads.response.customer.last_name;
          
        } catch (e) {
          // alert(
          //   'Unable to preserve session, probably due to being in private ' +
          //   'browsing mode.'
          // );
        }
        done();
      })
      .catch(payload => dispatch({
        type: SESSION_LOGIN,
        error: true,
        payload: {
          statusCode: payload.status, message: payload.statusText
        }
      }))
  );
}

export function logOut(session) {
  return (dispatch) => {
    // dispatch({ type: SESSION_LOGOUT });
    // deleteSession(session);
    dispatch({
      type: SESSION_LOAD, payload: { email: undefined, name: undefined, token: undefined }
    });
    updateHeaders({ Auth: undefined });
    try {
      localStorage.removeItem('email');
      localStorage.removeItem('name');
      localStorage.removeItem('token');
    } catch (e) {
      // ignore
    }
    //window.location.href = '/login'; // reload fully
  };
}
