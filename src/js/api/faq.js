import RequestWatcher from './request-watcher';
import { headers, parseJSON } from './utils';


export function fetchFaq() {
  const options = {
    headers: headers(),
    method: 'GET',
    body: {},
    mode: 'no-cors',
  };

  return fetch('https://nextverp.herokuapp.com/faq/list', options)
    .then(parseJSON);
}

