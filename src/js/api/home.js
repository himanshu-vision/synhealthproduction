import { requestWatcher } from './utils';

let dashboardWatcher;

export function watchhome() {
  dashboardWatcher = requestWatcher.watch('/api/task?status=Running');
  return dashboardWatcher;
}

export function unwatchhome() {
  if (dashboardWatcher) {
    dashboardWatcher.stop();
  }
}
