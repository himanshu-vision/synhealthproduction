import RequestWatcher from './request-watcher';

let _headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
  
};

export function headers(customHeader) {
  const token = localStorage.getItem('token');
  if (token) {
    return { ..._headers, 'x-header-token': token, ...customHeader };
  } else {
    return _headers;
  }
}

export function parseJSON(response) {
  if (response.ok) {
    return response.json();
  }
  return Promise.reject(response);
}

export function updateHeaders(newHeaders) {
  console.log('newHeadersnewHeaders', newHeaders);
  let token = newHeaders.Auth;
  _headers = { ..._headers, 'x-header-token': token };
  console.log(_headers);
  Object.keys(_headers).forEach(key => {
    if (undefined === _headers[key]) {
      delete _headers[key];
    }
  });
}

export const requestWatcher = new RequestWatcher();
