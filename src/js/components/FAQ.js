import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';

import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import Paragraph from 'grommet/components/Paragraph';
import Accordion from 'grommet/components/Accordion';
import AccordionPanel from 'grommet/components/AccordionPanel';

class FAQ extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { faqs } = this.props;
    const { intl } = this.context;
    console.log('faqsfaqsfaqsfaqs', faqs);

    return (
      <Box pad={'small'}>
        <Heading strong={true} truncate={true} tag="h3">
          FAQ
        </Heading>
        <Accordion>
          {faqs.map(item => (
            <AccordionPanel heading="First Title">
              <Paragraph>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </Paragraph>
            </AccordionPanel>
          ))}
        </Accordion>
      </Box>
    );
  }
}

export default FAQ;
