import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';

import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import Search from 'grommet/components/Search';
import TextInput from 'grommet/components/TextInput';
import FormField from 'grommet/components/FormField';


class SearchBar extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box pad={'small'}>
     
     <Search placeHolder='Search'
  inline={true}
  suggestions={['first', 'second', 'third', 'fourth']}
  size='medium'
  value=''
  // onSelect={...}
  // onDOMChange={...}
   />
   <FormField>
  <TextInput id='item1'
    name='item-1'
    value=''
    // onDOMChange={...}
    // onSelect={...}
    suggestions={['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight']} />
</FormField>
   
     
      </Box>
    );
  }
}

export default SearchBar;
