import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';

import Image from 'grommet/components/Image';
import Title from 'grommet/components/Title';
import Heading from 'grommet/components/Heading';
import Box from 'grommet/components/Box';
import Columns from 'grommet/components/Columns'

class Steps extends Component {
  constructor() {
    super();
    this.state = {};
  }

  render() {
    const { error, tasks } = this.props;
    const { intl } = this.context;

    let errorNode;
    let listNode;

    return (
      <Box
        pad={'small'}
        style={{ alignItems: 'center', marginTop: 100, marginBottom: 50 }}
      >
        <Heading strong={true} truncate={true} tag="h2">
          Get your helth test done in 4 easy steps
        </Heading>
        <Columns
          pad={'small'}
          direction={'column'}
          responsive={false}
          justify={'start'}
          masonry
          responsive
          size={'small'}
          maxCount={2}
          style={{
            justifyContent: 'space-around',
            marginTop: 100,
          }}
        >
          <CardDesign
            image="../img/onlinebook.svg"
            step={1}
            t1={'Book test/package'}
            t2={'Book Test/Package or Call/Chat with us for assistance'}
          />

          <CardDesign
            image="../img/callback.svg"
            step={2}
            t1={'Get a callback'}
            t2={'Confirm your details with our team'}
          />

          <CardDesign
            image="../img/bloodsample.svg"
            step={3}
            t1={'Get your sample collected'}
            t2={'Get your semple collected from doorstep'}
          />

          <CardDesign
            image="../img/getreport.svg"
            step={4}
            t1={'Collect your report'}
            t2={'Get your report online or offline'}
          />
        </Columns>
      </Box>
    );
  }
}

export default Steps;

const CardDesign = ({ image, t1, t2, step }) => (
  <Box
    style={{
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      margin: 40,
    }}
  >
    <Image
      alt="logo"
      size="small"
      src={image}
      style={{
        width: 120,
        height: 120,
        resizeMode: 'contain',
        marginBottom: 20,
      }}
    />
    <Heading strong={true} truncate={true} tag="h2">
      Step {step}
    </Heading>
    <Heading strong={true} truncate={true} tag="h3">
      {t1}
    </Heading>
    <Heading strong={true} truncate={true} tag="h4">
      {t2}
    </Heading>
  </Box>
);
