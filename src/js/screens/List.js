import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Anchor from 'grommet/components/Anchor';

import Split from 'grommet/components/Split';
import Sidebar from 'grommet/components/Sidebar';
import Box from 'grommet/components/Box';
import Search from 'grommet/components/Search';

import Header from 'grommet/components/Header';
import FormFields from 'grommet/components/FormFields';
import Button from 'grommet/components/Button';
import Select from 'grommet/components/Select';
import Card from 'grommet/components/Card';
import LoginForm from 'grommet/components/LoginForm';
import FormField from 'grommet/components/FormField';
import TextInput from 'grommet/components/TextInput';
import Article from 'grommet/components/Article';
import Section from 'grommet/components/Section';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Logo from 'grommet/components/icons/Grommet';
import Footers from '../components/Footer';
import Footer from 'grommet/components/Footer';
import NavControl from '../components/NavControl';
import SearchInput from 'grommet/components/SearchInput';

import Form from 'grommet/components/Form';
import { login } from '../actions/session';
import { navEnable } from '../actions/nav';
import { pageLoaded } from './utils';

class List extends Component {
  constructor() {
    super();
    // this._onSubmit = this._onSubmit.bind(this);
  }

  render() {
    // const { session: { error } } = this.props;

    return (
      <Article>
        <NavControl />
        <Header pad="medium">
          <Box flex={true} justify="end" direction="row" responsive={false}>
            <Search
              inline={true}
              fill={true}
              size="medium"
              suggestions={[
                {
                  value: 'first',
                  sub: 'alpha',
                  label: (
                    <Box direction="row" justify="between">
                      \n <span>\n first\n </span>\n{' '}
                      <span className="secondary">\n alpha\n </span>\n
                    </Box>
                  ),
                },
                {
                  value: 'second',
                  sub: 'beta',
                  label: (
                    <Box direction="row" justify="between">
                      \n <span>\n second\n </span>\n{' '}
                      <span className="secondary">\n beta\n </span>\n
                    </Box>
                  ),
                },
                {
                  value: 'third',
                  sub: 'gamma',
                  label: (
                    <Box direction="row" justify="between">
                      \n <span>\n third\n </span>\n{' '}
                      <span className="secondary">\n gamma\n </span>\n
                    </Box>
                  ),
                },
                {
                  value: 'fourth',
                  sub: 'delta',
                  label: (
                    <Box direction="row" justify="between">
                      \n <span>\n fourth\n </span>\n{' '}
                      <span className="secondary">\n delta\n </span>\n
                    </Box>
                  ),
                },
              ]}
              placeHolder="Search"
              dropAlign={{ top: 'bottom' }}
            />
          </Box>
        </Header>
        <Split flex="right">
          <Box
            colorIndex="neutral-1"
            justify="center"
            align="center"
            pad="medium"
          >
            <Select
              placeHolder="REFINE BY"
              inline={false}
              multiple={true}
              onSearch={false}
              options={['Package', 'Profile', 'Test']}
              value={[]}
              //onChange={...}
            />

            <Select
              placeHolder="Categories"
              inline={false}
              multiple={true}
              onSearch={false}
              options={[
                'Diabetic',
                'Thyroid',
                'Cardiac',
                'Pragancy',
                'Child',
                'care',
              ]}
              value={[]}
              //onChange={...}
            />
          </Box>
          <Box
            direction="row"
            wrap={true}
            justify="start"
            align="start"
            pad="medium"
          >
            <Card
              margin={'small'}
              thumbnail="/img/profile.jpg"
              label="Sample Label"
              heading="package Name"
              description="Sample description providing more details."
              link={<Anchor href="" label="Sample anchor" />}
            />
            <Card
              margin={'small'}
              thumbnail="/img/profile.jpg"
              label="Sample Label"
              heading="package Name"
              description="Sample description providing more details."
              link={<Anchor href="" label="Sample anchor" />}
            />
            <Card
              margin={'small'}
              thumbnail="/img/profile.jpg"
              label="Sample Label"
              heading="package Name"
              description="Sample description providing more details."
              link={<Anchor href="" label="Sample anchor" />}
            />
            <Card
              margin={'small'}
              thumbnail="/img/profile.jpg"
              label="Sample Label"
              heading="package Name"
              description="Sample description providing more details."
              link={<Anchor href="" label="Sample anchor" />}
            />
            <Card
              margin={'small'}
              thumbnail="/img/profile.jpg"
              label="Sample Label"
              heading="package Name"
              description="Sample description providing more details."
              link={<Anchor href="" label="Sample anchor" />}
            />
          </Box>
        </Split>
        <Footers />
      </Article>
    );
  }
}

export default List;
